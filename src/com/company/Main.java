package com.company;

class Main {

    /**
     * metoda main powinna implementowac algorytm do
     * jak najszybszego wyszukiwania wartosci
     * zmiennej digit z klasy QuizImpl (zakladamy ze
     * programista nie zna zawartosci klasy QuizImpl).
     * Nalezy zalozyc, ze pole digit w klasie QuizImpl
     * moze w kazdej chwili ulec zmianie. Do wyszukiwania
     * odpowiedniej wartosci nalezy wykorzystywac tylko
     * i wylacznie metode isCorrectValue - jesli metoda
     * przestanie rzucac wyjatki wowczas mamy pewnosc ze
     * poszukiwana zmienna zostala odnaleziona.
     */
    public static void main(String[] args) {

        Quiz quiz = new QuizImpl();
        int digit = 2; // zainicjuj zmienna
        int min = Quiz.MIN_VALUE;
        int max = Quiz.MAX_VALUE;

        for(int counter = 1; ;counter++) {

            try {
                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;

            } catch(Quiz.ParamTooLarge e) {
                System.out.println("Argument za duzy!!!");
                // implementacja logiki...
                max = digit;
                digit = (min+max)/2;

            } catch(Quiz.ParamTooSmall e) {
                System.out.println("Argument za maly!!!");
                // implementacja logiki...
                min = digit;
                digit = (min+max)/2;
            }
        }
    }
}
