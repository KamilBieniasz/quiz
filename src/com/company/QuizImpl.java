package com.company;

public class QuizImpl implements Quiz {
    private int digit;
    private int max = MAX_VALUE;
    private int min = MIN_VALUE;

    public QuizImpl() {
        this.digit = 254;	// zmienna moze ulegac zmianie!
    }

    @Override
    public void isCorrectValue(int value) throws ParamTooLarge, ParamTooSmall {
        if(digit < value){
            throw new ParamTooLarge();
        }
        else if(digit > value){
            throw new ParamTooSmall();
        }

    }

}
